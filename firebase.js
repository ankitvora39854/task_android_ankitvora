import firebase from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyBbhGHD16r-xAhK0AF2OxLqjsenHfWbits",
  authDomain: "shinesoft-6ff80.firebaseapp.com",
  projectId: "shinesoft-6ff80",
  storageBucket: "shinesoft-6ff80.appspot.com",
  messagingSenderId: "850323537181",
  appId: "1:850323537181:web:c2c8db20527c2f02f375f4",
  measurementId: "G-RKXPZFVZF0",
};

let Firebase = firebase.initializeApp(firebaseConfig);

// Initialize Firebase
export default Firebase;
