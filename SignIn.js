import React, { useState } from "react";
import { View, TextInput, Button } from "react-native";
import auth from "@react-native-firebase/auth";

function SignIn() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  return (
    <View>
      <TextInput
        placeholder="Enter Email"
        value={name}
        onChangeText={(email) => setName({ email })}
        autoCapitalize="none"
      />
      <TextInput
        placeholder="Enter Password"
        value={password}
        onChangeText={(password) => setPassword({ password })}
        secureTextEntry={true}
      />
      <Button title="Sign In" onPress={() => SignIn()} />
    </View>
  );
}

export default SignIn;
