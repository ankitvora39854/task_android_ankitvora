import React, { useState } from "react";
import { View, TextInput, Button } from "react-native";
import auth from "@react-native-firebase/auth";

function Signup() {
  const handleSignUp = () => {
    const { email, password } = this.state;
    Firebase.auth()
      .createUserWithEmailAndPassword(email, password)
      .then(() => this.props.navigation.navigate("Profile"))
      .catch((error) => console.log(error));
  };

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  return (
    <View>
      <TextInput
        placeholder="Enter Name"
        value={name}
        onChangeText={(name) => setName({ name })}
        autoCapitalize="none"
      />
      <TextInput
        placeholder="Enter Email"
        value={name}
        onChangeText={(email) => setName({ email })}
        autoCapitalize="none"
      />
      <TextInput
        placeholder="Enter Password"
        value={password}
        onChangeText={(password) => setPassword({ password })}
        secureTextEntry={true}
      />
      <Button title="Sign up" onPress={handleSignUp} />
    </View>
  );
}

export default Signup;
