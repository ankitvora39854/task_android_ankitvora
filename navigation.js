import React from "react";
import { createSwitchNavigator, createAppContainer } from "react-navigation";

import SignIn from "./SignIn";
import Signup from "./Signup";
import Profile from "./Profile";

const SwitchNavigator = createSwitchNavigator(
  {
    SignIn: {
      screen: SignIn,
    },
    Signup: {
      screen: Signup,
    },
    Profile: {
      screen: Profile,
    },
  },
  {
    initialRouteName: "Login",
  }
);

export default createAppContainer(SwitchNavigator);
